/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 *
 * @author Josua_Marpaung
 */
public enum DataMakanan {
    Nasi_Goreng, Mie_Goreng;
    
    public String toString(){
        switch(this){
            case Nasi_Goreng:
                return "12000";
            case Mie_Goreng:
                return "10000";
            default: return "Anda Tidak Memesan Apapun";
        }
    }

}
