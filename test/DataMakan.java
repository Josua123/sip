/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 *
 * @author Josua_Marpaung
 */
public abstract class DataMakan <T> {
    private T nama, harga;

    public T getNama() {
        return nama;
    }

    public void setNama(T nama) {
        this.nama = nama;
    }

    public T getHarga() {
        return harga;
    }

    public void setHarga(T harga) {
        this.harga = harga;
    }
    
}
