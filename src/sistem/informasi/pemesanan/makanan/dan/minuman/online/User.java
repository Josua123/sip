/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sistem.informasi.pemesanan.makanan.dan.minuman.online;

/**
 *
 * @author Josua_Marpaung
 */
public abstract class User <T>{
    private T username, password;

    public T getUsername() {
        return username;
    }

    public void setUsername(T username) {
        this.username = username;
    }

    public T getPassword() {
        return password;
    }

    public void setPassword(T password) {
        this.password = password;
    }
}
