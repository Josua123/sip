/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sistem.informasi.pemesanan.makanan.dan.minuman.online;

/**
 *
 * @author Josua_Marpaung
 */

import java.io.*;
import java.util.*;
import java.time.*;
import java.time.format.DateTimeFormatter;

public class AppPemesananOnline {

    public static void Login(){
        System.out.println("\n**==** SELAMAT DATANG **==** ");
        System.out.println("(1) Login Admin");
        System.out.println("(2) Login Pelanggan");
        System.out.println("(3) Daftar Pelanggan");
        System.out.println("(0) Keluar");
        System.out.println("Ketik nomor untuk melanjutkan : ");
    }
    
    public static void DashboardAdmin(){
        System.out.println("\n**==** SELAMAT DATANG  ADMIN **==** ");
        System.out.println("(1) Kelola Pemesanan");
        System.out.println("(0) Kembali");
        System.out.println("Ketik nomor untuk melanjutkan : ");
    }
    
    public static void DashboardPelanggan(){
        System.out.println("\n**==** SELAMAT DATANG PELANGGAN **==** ");
        System.out.println("(1) Menu Makanan");
        System.out.println("(2) Menu Minuman");
        System.out.println("(3) Daftar Pemesanan");
        System.out.println("(0) Kembali");
        System.out.println("Ketik nomor untuk melanjutkan : ");
    }
    
    public static void MenuMakanan(){
        System.out.println("\n|----------------- DAFTAR MENU MAKANAN ----------------|\n"
                         + "| (1) Nasi Goreng    || (2) Mie Goreng  || (3) Bakpau  |\n"
                         + "| (4) Bihun Goreng   || (5) Ifu Mie     || (6) Pangsit |\n"
                         + "|--------------------- (0) Keluar ---------------------|");
        System.out.println(">>> Ketik nomor untuk melanjutkan : ");
    }
    
    public static void MenuMinuman(){
        System.out.println("\n|------------------- DAFTAR MENU MINUMAN --------------------|\n"
                         + "| (1) Teh Manis || (2) Air Putih          || (3) Jus Alpukat |\n"
                         + "| (4) Jus Jeruk || (5) Jus Terong Belanda || (6) Jus Markisa |\n"
                         + "|------------------------ (0) Keluar ------------------------|");
        System.out.println(">>> Ketik nomor untuk melanjutkan : ");
    }
    public static void main(String[] args){ 
        
    BufferedWriter save, saveUpd, saveDel, cetak; 
    BufferedReader scan = new BufferedReader(new InputStreamReader(System.in)); 
    Scanner input = new Scanner(System.in); //variabel untuk menerima inputan user
    String pelangganTemp, pelangganNow="";
    
    //variabel
    int Login, DashboardAdmin, DashboardPelanggan, MenuMakanan, MenuMinuman, admin=0, pelanggan=0; 
    String usernameAdmin = "admin", passwordAdmin = "admin";
    DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
    DateTimeFormatter dtf2 = DateTimeFormatter.ofPattern("dd-MM-yyyy HH.mm");
    LocalDateTime now = LocalDateTime.now();
    LocalDateTime pemesananDate = LocalDateTime.now();
 
    try{
        do{
            Login();
            Login = input.nextInt();

            if(Login == 1){
                System.out.println(""); 
                System.out.print("Masukkan username: "); 
                String username = scan.readLine(); 
                System.out.print("Masukkan password: "); 
                String password = scan.readLine();  
                
                ArrayList<String> listAdminTxt = new ArrayList<>(); 
                try{
                    File data = new File ("Data Pemesanan/Admin.txt"); 
                    Scanner getData = new Scanner(data);  

                    listAdminTxt.clear();
                    while(getData.hasNextLine()) {
                        listAdminTxt.add(getData.nextLine()); 
                    }
                    
                    //mencetak data 
                    for (int i=0; i<listAdminTxt.size(); i++){
                        if(listAdminTxt.get(i).split(",")[0].split(": ")[1].equals(username) && listAdminTxt.get(i).split(",")[1].split(": ")[1].equals(password)){
                            admin = 1;
                            break;
                        }
                    }
                    listAdminTxt.clear(); 
                    
                    if(admin == 1){
                        System.out.println("Berhasil masuk!");
                        break;
                    } 
                    else{
                        System.out.println("ERROR -- Gagal masuk"); 
                    }
                }
                catch(IOException e){
                } 
            }
            
            else if(Login == 2){ 
                System.out.print("\nMasukkan username: "); 
                String username = input.next();
                System.out.print("Masukkan password: "); 
                String password = scan.readLine();  
              
                ArrayList<String> listPelangganTxt = new ArrayList<>(); 
                try{
                    File data = new File ("Data Pemesanan/Pelanggan.txt");
                    Scanner getData = new Scanner(data);  

                    listPelangganTxt.clear();
                    while(getData.hasNextLine()) { 
                        listPelangganTxt.add(getData.nextLine()); 
                    }
                    
                    //mencetak data 
                    for (int i=0; i<listPelangganTxt.size(); i++){
                        if(listPelangganTxt.get(i).split(",")[5].split(": ")[1].equals(username) && listPelangganTxt.get(i).split(",")[6].split(": ")[1].equals(password)){
                            pelanggan = 1;
                            pelangganTemp = listPelangganTxt.get(i).split(",")[5];
                            pelangganNow = pelangganTemp.split(": ")[1];
                            break;
                        }
                    }
                    listPelangganTxt.clear(); 
                    
                    if(pelanggan == 1){
                        System.out.println("Berhasil masuk!");
                        break;
                    } 
                    else{
                        System.out.println("ERROR -- Gagal masuk"); 
                    }
                }
                catch(IOException e){
                } 
            }
            
            else if(Login == 3){
                System.out.println(""); 
                System.out.println("Masukkan data diri anda : ");
                System.out.print("Nama: "); 
                String nama = scan.readLine(); 
                System.out.print("Umur: "); 
                String umur = scan.readLine();  
                System.out.print("Gender: "); 
                String gender = scan.readLine();
                System.out.print("Alamat: "); 
                String alamat = scan.readLine();
                System.out.print("Email: "); 
                String email = scan.readLine();
                System.out.print("Username: "); 
                String username = scan.readLine(); 
                System.out.print("Password: "); 
                String password = scan.readLine();
                
                ArrayList<String> listPelangganTxt = new ArrayList<>(); //List menampung dari txt file
                try{
                    File data = new File ("Data Pemesanan/Pelanggan.txt"); //Objek file yang akan dibuka
                    Scanner getData = new Scanner(data); //Untuk membaca data dari file 

                    listPelangganTxt.clear();//menghapus list, agar ketika list dipanggil ulang, element tidak bertindih
                    while(getData.hasNextLine()) { //membaca seluruh isi file
                        listPelangganTxt.add(getData.nextLine()); //save data dari txt file ke arraylist
                    }

                    //cek apakah inputan sama dengan yang terdaftar
                    int temp=0;
                    for (int i=0; i<listPelangganTxt.size(); i++){
                        if(listPelangganTxt.get(i).split(",")[5].split(": ")[1].equals(username) || listPelangganTxt.get(i).split(",")[4].split(": ").equals(email)){
                            temp = 1;
                        }
                    }
                    listPelangganTxt.clear(); 
                    
                    if(temp == 1){
                        System.out.println(">>>Maaf, Username/Email Sudah Pernah Terdaftar");
                        break;
                    }
                    
                    save = new BufferedWriter (new FileWriter(data, true)); //true, berarti data file lama ditambahkan

                    Pelanggan dataPelanggan = new Pelanggan(nama, umur, gender, alamat, email, username, password);

                    save.write(dataPelanggan.toString()); //save data ke txt dalam format toString
                    save.newLine(); 
                    save.close(); //close I/O
                    System.out.println(">>>Daftar Berhasil");
                }catch(IOException e){ 
                } 
            } 
        }
        while(Login != 0); 
        
        if(admin == 1){
            do{
                DashboardAdmin();
                DashboardAdmin = input.nextInt();
                switch(DashboardAdmin){
                    
                }
            }while (DashboardAdmin !=0);
        }
        
        else if(pelanggan == 1){
            do{
                DashboardPelanggan();
                DashboardPelanggan = input.nextInt();
                switch(DashboardPelanggan){
                    case 1:
                        do{
                            MenuMakanan();
                            MenuMakanan = input.nextInt();
                            switch(MenuMakanan){
                                case 1 :
                                    ArrayList<nasiGoreng> listnasiGoreng = new ArrayList<>();
                                    System.out.println("\nAnda Memesan Nasi Goreng"); 
                                    System.out.println("Harga : 12000/pcs");
                                    System.out.println("Berapa jumlah pesanan yang anda inginkan : ");
                                    String jumlahPemesanan = input.next();
                                    Date tanggal = new Date();
                                        
                                        listnasiGoreng.clear(); 
                                        listnasiGoreng.add(new nasiGoreng("Nasi Goreng", 12000, jumlahPemesanan, pelangganNow, tanggal));
                                        for (nasiGoreng list : listnasiGoreng){
                                            try{
                                                File data = new File ("Data Pemesanan/Makanan.txt"); //Objek file yang akan dibuka
                                                save = new BufferedWriter (new FileWriter(data, true)); //true, berarti data file lama ditambahkan
                                                save.write(list.toString()); //save data ke txt dalam format toString
                                                save.newLine();
                                                save.close(); //close I/O
                                                System.out.println(">>>Tambah Data Berhasil");
                                            }
                                            catch(IOException e){
                                            }
                                        }
                                        listnasiGoreng.clear();
                                        break;
                                case 2:
                                        ArrayList<mieGoreng> listmieGoreng = new ArrayList<>();
                                        System.out.println("\nAnda Memesan Mie Goreng");
                                        System.out.println("Harga : 10000/pcs");
                                        System.out.println("Berapa jumlah pesanan yang anda inginkan : ");
                                        String jumlahPemesanan2 = input.next();
                                        Date tanggal2 = new Date();
                                        
                                        listmieGoreng.clear();
                                        listmieGoreng.add(new mieGoreng("Mie Goreng", 12000, jumlahPemesanan2, pelangganNow, tanggal2));
                                        for (mieGoreng list : listmieGoreng) {
                                            try{
                                                File data = new File ("Data Pemesanan/Makanan.txt"); //Objek file yang akan dibuka
                                                save = new BufferedWriter (new FileWriter(data, true)); //true, berarti data file lama ditambahkan
                                                save.write(list.toString()); //save data ke txt dalam format toString
                                                save.newLine();
                                                save.close(); //close I/O
                                                System.out.println(">>>Pesanan Telah Ditambahkan");
                                            }
                                            catch(IOException e){
                                            }
                                        }
                                        listmieGoreng.clear();
                                        break;
                                        
                                case 3 :
                                    ArrayList<bakpau> listBakpau = new ArrayList<>();
                                        System.out.println("\nAnda Memesan Mie Goreng");
                                        System.out.println("Harga : 10000/pcs");
                                        System.out.println("Berapa jumlah pesanan yang anda inginkan : ");
                                        String jumlahPemesanan3 = input.next();
                                        Date tanggal3 = new Date();
                                        
                                        listBakpau.clear();
                                        listBakpau.add(new bakpau("Bakpau", 7000, jumlahPemesanan3, pelangganNow, tanggal3));
                                        for (bakpau list : listBakpau) {
                                            try{
                                                File data = new File ("Data Pemesanan/Makanan.txt"); //Objek file yang akan dibuka
                                                save = new BufferedWriter (new FileWriter(data, true)); //true, berarti data file lama ditambahkan
                                                save.write(list.toString()); //save data ke txt dalam format toString
                                                save.newLine();
                                                save.close(); //close I/O
                                                System.out.println(">>>Pesanan Telah Ditambahkan");
                                            }
                                            catch(IOException e){
                                            }
                                        }
                                        listBakpau.clear();
                                        break;
                            }
                        }while(MenuMakanan !=0);
                        break;
                        
                    case 2 :
                        do{
                            MenuMinuman();
                            MenuMinuman = input.nextInt();
                            switch(MenuMinuman){
                                case 1: 
                                    ArrayList<tehManis> listtehManis = new ArrayList<>();
                                    ArrayList<String> listtehManisTxt = new ArrayList<>();
                                    System.out.println("\nAnda Memesan Teh Manis");
                                    System.out.println("Harga : 5000/pcs");
                                    System.out.println("Berapa jumlah pesanan yang anda inginkan : ");
                                    String jumlahPemesanan3 = input.next();
                                    Date tanggal = new Date(); 
                                        
                                        listtehManis.clear(); 
                                        listtehManis.add(new tehManis("Teh Manis", 5000, jumlahPemesanan3, pelangganNow, tanggal));
                                        for (tehManis list : listtehManis){
                                            try{
                                                File data = new File ("Data Pemesanan/" + dtf2.format(pemesananDate)+ " - " + pelangganNow +".txt"); //Objek file yang akan dibuka
                                                save = new BufferedWriter (new FileWriter(data, true)); //true, berarti data file lama ditambahkan
                                                save.write(list.toString()); //save data ke txt dalam format toString
                                                save.newLine();
                                                save.close(); //close I/O
                                                System.out.println(">>>Pesanan Telah Ditambahkan");
                                            }
                                            catch(IOException e){
                                            }
                                        }
                            }
                        }while(MenuMinuman !=0);
                        break;
                }
            }while (DashboardPelanggan !=0);
        }
    }
    catch(InputMismatchException e){
        System.out.println("ERROR -- Maaf, ada yang salah dengan inputan Anda");
    } 
    catch(IOException e){
        System.out.println("ERROR -- Maaf, telah terjadi kesalahan pada File");
    }
    catch(ArrayIndexOutOfBoundsException e){
        System.out.println("ERROR -- Data yang Hendak ditampilkan diluar Batas Indeks<array>");
    }
    catch(IndexOutOfBoundsException e){
        System.out.println("ERROR -- Data yang diminta diluar Batas Indeks<array>");
    }
    System.out.println(""); 
    System.out.println("\n***Terimakasih Telah Datang Berkunjung*** \n"); 
 }
}
