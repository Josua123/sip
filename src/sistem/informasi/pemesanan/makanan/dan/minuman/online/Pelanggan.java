/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sistem.informasi.pemesanan.makanan.dan.minuman.online;

/**
 *
 * @author Josua_Marpaung
 */
public class Pelanggan <T> extends User{
    private T nama, umur, gender, alamat, email;
    
    public Pelanggan(T nama, T umur, T gender, T alamat, T email, T username, T password){
        this.nama = nama;
        this.umur = umur;
        this.gender = gender;
        this.alamat = alamat;
        this.email = email;
        super.setUsername(username);
        super.setPassword(password);
    }
    
    @Override 
    public String toString(){
       return "Nama: " +nama+
               ", Umur: " +umur+
               ", Gender: " +gender+
               ", Alamat: " +alamat+
               ", email: " +email+ 
               ", Username: " +super.getUsername()+
               ", Password: " +super.getPassword();
    }
}
