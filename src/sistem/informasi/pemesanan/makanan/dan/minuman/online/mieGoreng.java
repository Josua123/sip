/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sistem.informasi.pemesanan.makanan.dan.minuman.online;

/**
 *
 * @author Josua_Marpaung
 */
public class mieGoreng <T> extends abstractPemesanan{
    private T nama, harga, jumlahPemesanan;

    public mieGoreng(T nama, T harga, T jumlahPemesanan, T pemesan, T tanggal) {
        this.nama = nama;
        this.harga = harga;
        this.jumlahPemesanan = jumlahPemesanan;
        super.setPemesan(pemesan);
        super.setTanggal(tanggal);
    }

    public T getNama() {
        return nama;
    }

    public void setNama(T nama) {
        this.nama = nama;
    }

    public T getHarga() {
        return harga;
    }

    public void setHarga(T harga) {
        this.harga = harga;
    }

    public T getJumlahPemesanan() {
        return jumlahPemesanan;
    }

    public void setJumlahPemesanan(T jumlahPemesanan) {
        this.jumlahPemesanan = jumlahPemesanan;
    }

    @Override
    public String toString(){
        return "Makanan : " +nama+
                ", Harga : " +harga+
                ", Jumlah Pemesanan : " +jumlahPemesanan+
                ", Pemesan : " +super.getPemesan()+
                ", Tanggal Pemesanan : " +super.getTanggal();
    }
}
