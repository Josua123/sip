/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sistem.informasi.pemesanan.makanan.dan.minuman.online;

/**
 *
 * @author Josua_Marpaung
 */
public abstract class abstractPemesanan <T> {
    public T pemesan, tanggal;
    private statusPemesanan sp;

    public T getPemesan() {
        return pemesan;
    }

    public void setPemesan(T pemesan) {
        this.pemesan = pemesan;
    }

    public T getTanggal() {
        return tanggal;
    }

    public void setTanggal(T tanggal) {
        this.tanggal = tanggal;
    }

    public statusPemesanan getSp() {
        return sp;
    }

    public void setSp(statusPemesanan sp) {
        this.sp = sp;
    }

    
    
}
